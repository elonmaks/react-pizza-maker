import React, { Component } from 'react';
import Bases from './components/Bases/Bases';
import Preview from './components/Preview/Preview';

class App extends Component {
  state = {
    show: true
  }

  componentDidMount() {
    this.onPreviewHandle();
  }

  onPreviewHandle = () => {
    setTimeout(() => {
      this.setState({ show: false })
    }, 5000)
  }



  render() {
    return (
      <div className="App">
        {this.state.show ? <Preview /> : <Bases />}
      </div>
    );
  }
}

export default App;
