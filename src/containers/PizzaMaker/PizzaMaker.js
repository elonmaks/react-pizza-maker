import React, { Component } from 'react'
import { connect } from 'react-redux';
import Pizza from '../../components/Pizza/Pizza';
import BuildControls from '../../components/Pizza/PizzaIngredient/BuidControlls/BuildControls';
import Price from '../../components/Price/Price';
import IngredientsConstructor from '../../components/IngredientsConstructor/IngredientsConstructor';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Pizza/OrderSummary/OrderSummary';
import * as pizzaMakerActions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../components/withErrorHandler/withErrorHandler';
import axios from '../../axios-orders';

class PizzaMaker extends Component {
    state = {
        purchasing: false,
    }

    componentDidMount() {
        this.props.onInitIngredients();
    }

    purchaseCancelHandler = () => {
        this.setState({ purchasing: false })
    }

    purchaseContinueHandler = () => {
        this.setState({ loading: true });

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
        }
        axios.post('/orders.json', order)
            .then(response => this.setState({ loading: false, purchasing: false }))
            .catch(error => this.setState({ loading: false, purchasing: false }));
        alert('Your order complete!');
    }

    render() {
        const {
            ingredients,
            onIngredientDropped,
            bases, dragging,
            onIngredientAdded,
            onIngredientRemoved,
            error,
            totalPrice } = this.props;

        let orderSummary = null;
        let pizza = error ? <p>Ingredients can't be loaded</p> : <Spinner />

        if (ingredients) {
            pizza = (
                <React.Fragment>
                    <IngredientsConstructor
                        ingredients={ingredients}
                        dragged={onIngredientDropped} />
                    <Pizza bases={bases} />
                    <BuildControls
                        show={dragging}
                        ingredientAdded={onIngredientAdded}
                        ingredientRemoved={onIngredientRemoved}
                        ingredients={ingredients}
                    />
                    <Price price={totalPrice.toFixed(2)} />
                </React.Fragment>
            );
            orderSummary = <OrderSummary
                ingredients={ingredients}
                price={totalPrice}
                purchaseCanceled={this.purchaseCancelHandler}
                purchaseContinued={this.purchaseContinueHandler} />
        }

        return (
            <React.Fragment>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {pizza}
            </React.Fragment>
        )
    }
}



const mapStateToProps = state => {
    return {
        bases: state.bases,
        ingredients: state.ingredients,
        totalPrice: state.totalPrice,
        dragging: state.purchasing,
        error: state.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: ingredient => dispatch(pizzaMakerActions.addIngredient(ingredient)),
        onIngredientRemoved: ingName => dispatch(pizzaMakerActions.removeIngredient(ingName)),
        onIngredientDropped: ingName => dispatch(pizzaMakerActions.addIngredient(ingName)),
        onInitIngredients: () => dispatch(pizzaMakerActions.initIngredients())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(PizzaMaker, axios));
