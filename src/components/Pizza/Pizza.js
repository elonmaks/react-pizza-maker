import React from 'react';
import './Pizza.css';
import PizzaIngredient from './PizzaIngredient/PizzaIngredient';

const Pizza = (props) => {

    const transformObjectIntoArray = (object) => {
        return Object.keys(object)
            .map(ingredientKey => [...Array(object[ingredientKey])]
                .map((_, index) => (<PizzaIngredient key={ingredientKey + index} type={ingredientKey} />)))
            .reduce((array, element) => array.concat(element), []);
    }

    // const transformedIngredients = transformObjectIntoArray(props.ingredients);
    const transformedBases = transformObjectIntoArray(props.bases);


    return (
        <div className="Pizza">
            {transformedBases}
            {/* {transformedIngredients} */}
        </div>
    )
}

export default Pizza
