import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-pizza-maker-df6b1.firebaseio.com/'
})

export default instance;