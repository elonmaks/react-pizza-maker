import * as actionTypes from '../actions/actionTypes';

const initialState = {
    bases: {
        baseTomato: 0,
        baseUsual: 0,
        baseThin: 0
    },
    ingredients: null,
    error: false,
    totalPrice: 2.99,
    purchasing: false,
};

const INGREDIENT_PRICES = {
    mozzarella: 0.45,
    salami: 1.35,
    sturgeon: 1.2,
    bulgarian: 0.75,
    hunting: 1.1,
    meat: 1.5,
    mushrooms: 0.75,
    olives: 0.9,
    pepperoni: 1.2,
    pineapple: 1.3,
    prosciutto: 25,
    salmon: 3,
    shrimp: 3.15,
    tomato: 0.3,
    tuna: 2.65,
    cucumber: 0.95,
    corn: 0.2,
    arugula: 0.65
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            console.log('reducer', action);
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredient]: {
                        ...action.ingredient,
                        [action.ingredient.count]: state.ingredients[action.ingredient.count] + 1
                    }
                    //  state.ingredients[action.ingredientName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
                purchasing: true,
            };

        case actionTypes.REMOVE_INGREDIENT:
            const sum = Object.keys(state.ingredients)
                .map(ingredientsKey => state.ingredients[ingredientsKey])
                .reduce((sum, element) => sum + element, 0)
            const checkForIngredientsAmount = sum - 1 > 0;

            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
                purchasing: checkForIngredientsAmount,
            };

        case actionTypes.ADD_BASE:
            console.log(state.bases)
            return {
                ...state,
                bases: {
                    ...state.bases,
                    [action.baseName]: state.bases[action.baseName] + 1
                }
            }

        case actionTypes.SET_INGREDIENTS:
            return {
                ...state,
                ingredients: {
                    mozzarella: action.ingredients.mozzarella,
                    arugula: action.ingredients.arugula
                    // salami: action.ingredients.salami,
                    // sturgeon: action.ingredients.sturgeon,
                    // bulgarian: action.ingredients.bulgarian,
                    // hunting: action.ingredients.hunting,
                    // meat: action.ingredients.meat,
                    // mushrooms: action.ingredients.mushrooms,
                    // olives: action.ingredients.olives,
                    // pepperoni: action.ingredients.pepperoni,
                    // pineapple: action.ingredients.pineapple,
                    // prosciutto: action.ingredients.prosciutto,
                    // salmon: action.ingredients.salmon,
                    // shrimp: action.ingredients.shrimp,
                    // tomato: action.ingredients.tomato,
                    // tuna: action.ingredients.tuna,
                    // cucumber: action.ingredients.cucumber,
                    // corn: action.ingredients.corn,
                    // arugula: action.ingredients.arugula,
                },
                error: false,
            };

        case actionTypes.FETCH_INGREDIENTS_FAILED:
            return {
                ...state,
                error: true
            }

        default:
            return state;
    }
};

export default reducer;